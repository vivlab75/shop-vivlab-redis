const sanitize = require("./sanitize");
const algoliasearch = require("algoliasearch");
const client = algoliasearch(process.env.ALGOLIA_APPID, process.env.ALGOLIA_ADMINKEY);

async function saveObject({ payload, stripeAccount }) {
  const index = initIndex(stripeAccount);
  const data = await index.saveObject(filterObject(payload));
  return data;
}

async function saveObjects({ payload, stripeAccount }) {
  const index = initIndex(stripeAccount);
  const objects = [];

  for (const e of payload) {
    objects.push(filterObject(e));
  }

  await index.saveObjects(objects);
}

async function configureIndex({ stripeAccount }) {
  const index = initIndex(stripeAccount);

  const defaultRanking = [
    "typo",
    "geo",
    "words",
    "filters",
    "proximity",
    "attribute",
    "exact",
    "custom"
  ];
  const rankingList = [];
  try {
    const { ranking } = await index.getSettings();
    rankingList.push(...ranking.filter(e => defaultRanking.includes(e) === false));

    // eslint-disable-next-line no-empty
  } catch (e) {
  } finally {
    if (rankingList.length === 0) {
      rankingList.push("desc(skus.ts)");
    }
  }

  await index.setSettings({
    searchableAttributes: [
      "name",
      "description",
      "skus.identifier",
      "skus.variants.couleur",
      "skus.variants.capacité",
      "skus.variants.taille",
      "skus.variants.mémoire",
      "taxonomy_label.lvl0",
      "taxonomy_label.lvl1",
      "brand_label"
    ],
    disableTypoToleranceOnAttributes: ["skus.identifier"],
    ranking: [...rankingList, ...defaultRanking],
    attributesForFaceting: [
      "state_active",
      "brand",
      "searchable(brand_label)",
      "category",
      "hierarchy",
      "identifier",
      "skus.active",
      "skus.discount",
      "skus.inventory_quantity",
      "skus.price",
      "skus.variants.couleur",
      "prices.active",
      "subcategory",
      "tag",
      "taxonomy.lvl0",
      "taxonomy.lvl1",
      "taxonomy_label.lvl0",
      "taxonomy_label.lvl1",
      "type"
    ],
    unretrievableAttributes: ["description"]
  });
}

function filterObject(e) {
  const description = e.description ? sanitize.htmlToText(e.description).slice(0, 3000) : null;
  if (e.skus && e.skus.length) {
    const skus = e.skus.slice(0, 10);
    return {
      ...e,
      description,
      skus: skus.map(_ => ({
        sku: _.sku,
        ts: _.ts,
        active: _.active,
        price: _.price,
        inventory_quantity: _.inventory_quantity,
        discount: _.discount,
        identifier: _.identifier,
        variants: _.variants
      }))
    };
  }
  if (e.prices && e.prices.length) {
    const prices = e.prices.slice(0, 10);
    return {
      ...e,
      description,
      prices: prices.map(_ => ({
        price: _.price,
        ts: _.ts,
        active: _.active,
        unit_amount: _.unit_amount,
        interval: _.interval,
        discount: _.discount,
        variants: _.variants
      }))
    };
  }
  return {
    ...e,
    description,
    [e.metadata.type === "plan" ? "prices" : "skus"]: []
  };
}

function initIndex(stripeAccount) {
  const prefix = process.env.NODE_ENV === "production" ? "prod" : "dev";
  return client.initIndex(`${prefix}_${stripeAccount}_products`);
}

module.exports = {
  saveObject,
  saveObjects,
  configureIndex,
  initIndex
};
