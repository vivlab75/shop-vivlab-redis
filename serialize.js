const factory = require("shop-vivlab-factory");
const util = require("./util");

function serializeGood({ skus, product, taxonomy }) {
  const to = "products";
  return {
    ...productTransform({ subproducts: skus, product, to, taxonomy }),
    skus: skus.map(sku => ({
      ...skuTransform({ sku, product, to }),
      attach_file: sku.metadata.attach_file
    }))
  };
}

function serializeEvent({ skus, product, taxonomy }) {
  const to = "events";
  const metadata = product.metadata;
  return {
    ...productTransform({ subproducts: skus, product, to, taxonomy }),
    start_date: metadata.start_date,
    start_time: metadata.start_time,
    end_time: metadata.end_time,
    place_name: metadata.place_name,
    address_line1: metadata.address_line1,
    address_postal_code: metadata.address_postal_code,
    address_city: metadata.address_city,
    address_country: metadata.address_country,
    skus: skus.map(sku => skuTransform({ sku, product, to }))
  };
}

function serializePlan({ prices, product, taxonomy }) {
  const to = "plans";
  return {
    ...productTransform({ subproducts: prices, product, to, taxonomy }),
    prices: prices.map(price => priceTransform({ price, product, to }))
  };
}

function productTransform({ subproducts, product, to, taxonomy = {} }) {
  const metadata = product.metadata;

  const default_price = { unit: null, interval: null };

  if (product.metadata.type === "plan") {
    const min = Math.min(...subproducts.filter(e => e.active).map(e => e.unit_amount));
    const sub = subproducts.find(e => e.active && e.unit_amount === min);
    Object.assign(default_price, { unit: sub.unit_amount, interval: sub.recurring.interval });
  } else {
    Object.assign(default_price, {
      unit: Math.min(...subproducts.filter(e => e.active).map(e => e.price))
    });
  }

  const default_link = `/${to}/${factory.util.uniformDelims(product.name)}?item=${
    subproducts[0].id
  }`;
  const default_amount =
    factory.util.pricify(default_price.unit) + recurringInterval(default_price.interval);

  const default_inventory_quantity = Math.max(...subproducts.map(e => calcQuantity(e)));
  const default_discount = util.calcDiscount(default_price.unit, metadata.initial);

  const type = metadata.type;
  const tag = metadata.tag;
  const initial = util.parseInteger(metadata.initial);
  const extended_mandatory = util.parseBoolean(metadata.extended_mandatory);
  const extended = util.parseArray(metadata.extended);
  const is_free = util.parseBoolean(metadata.is_free);
  const price_substitute = metadata.price_substitute;
  const attach_file = metadata.attach_file || null;
  const amount_initial = Number(metadata.initial) ? factory.util.pricify(metadata.initial) : null;
  const sku_type = metadata.sku_type;

  return {
    active: product.active,
    state_active: product.active && subproducts.some(e => e.active),
    product: product.id,
    images: product.images,
    name: product.name,
    description: product.description,
    attributes: product.attributes,
    default_price: default_price.unit,
    default_link,
    default_amount,
    default_inventory_quantity,
    default_discount,
    type,
    tag,
    initial,
    extended_mandatory,
    extended,
    is_free,
    price_substitute,
    attach_file,
    amount_initial,
    sku_type,
    ...productTaxonomy({ product, taxonomy })
  };
}

function productTaxonomy({ product, taxonomy = {} }) {
  const { taxonomy_category, taxonomy_subcategory, taxonomy_brand } = product.metadata;
  const category = util.parseInteger(taxonomy_category) || 999;
  const subcategory = util.parseInteger(taxonomy_subcategory) || 999;
  const brand = util.parseInteger(taxonomy_brand) || 999;

  const hierarchies = taxonomy.hierarchies || [];
  const brands = taxonomy.brands || [];

  const cat = category !== 999 && hierarchies.find(e => e.id === category);
  const sub = subcategory !== 999 && cat && cat.subcategories.find(e => e.id === subcategory);
  const br = brand !== 999 && brands.find(e => e.id === brand);

  const taxonomyLabelLvl0 = cat ? cat.value : "Autres";
  const taxonomyLabelLvl1 = sub ? sub.value : "Autres";
  const brandLabel = br ? br.value : "Autres";

  const taxonomyLevel = {};
  const taxonomyLabelLevel = {};

  if (category !== 999) {
    Object.assign(taxonomyLevel, { lvl0: category });
    Object.assign(taxonomyLabelLevel, { lvl0: taxonomyLabelLvl0 });
    if (subcategory !== 999) {
      Object.assign(taxonomyLevel, { lvl1: `${category} > ${subcategory}` });
      Object.assign(taxonomyLabelLevel, { lvl1: `${taxonomyLabelLvl0} > ${taxonomyLabelLvl1}` });
    }
  }
  return {
    category,
    subcategory,
    hierarchy: `${category}:${subcategory}`,
    taxonomy: taxonomyLevel,
    taxonomy_label: taxonomyLabelLevel,
    brand,
    brand_label: brandLabel
  };
}

function skuTransform({ sku, product, to }) {
  const metadata = product.metadata;
  return {
    link: `/${to}/${factory.util.uniformDelims(product.name)}?item=${sku.id}`,
    ts: Math.max(...[product.created, product.updated, sku.created, sku.updated].filter(e => e)),
    active: sku.active,
    image: sku.image,
    amount: factory.util.pricify(sku.price),
    sku: sku.id,
    price: sku.price,
    inventory_type: sku.inventory.type,
    inventory_quantity: calcQuantity(sku),
    identifier: sku.metadata.identifier,
    discount: util.calcDiscount(sku.price, metadata.initial),
    variants: sku.attributes
  };
}

function priceTransform({ price, product, to }) {
  const metadata = product.metadata;
  return {
    link: `/${to}/${factory.util.uniformDelims(product.name)}?item=${price.id}`,
    ts: Math.max(
      ...[product.created, product.updated, price.created, price.updated].filter(e => e)
    ),
    active: price.active,
    image: price.metadata.image || null,
    amount: factory.util.pricify(price.unit_amount) + recurringInterval(price.recurring.interval),
    price: price.id,
    unit_amount: price.unit_amount,
    inventory_type: "infinite",
    inventory_quantity: 9999,
    start: util.parseInteger(price.metadata.start),
    iteration: util.parseInteger(price.metadata.iteration),
    interval: price.recurring.interval,
    discount: util.calcDiscount(price.unit_amount, metadata.initial),
    variants: util.parseObject(price.metadata.attributes)
  };
}

function calcQuantity(sku) {
  try {
    return sku.inventory.type === "finite" ? sku.inventory.quantity : 9999;
  } catch {
    return 9999;
  }
}

function recurringInterval(interval) {
  return interval === "day"
    ? " par jour"
    : interval === "week"
    ? " par semaine"
    : interval === "month"
    ? " par mois"
    : interval === "year"
    ? " par an"
    : "";
}

module.exports = function ({ skus, prices, product, taxonomy }) {
  if (product && product.metadata.type === "product" && skus.length) {
    return serializeGood({ skus, product, taxonomy });
  }
  if (product && product.metadata.type === "event" && skus.length) {
    return serializeEvent({ skus, product, taxonomy });
  }
  if (product && product.metadata.type === "plan" && prices.length) {
    return serializePlan({ prices, product, taxonomy });
  }
  return {};
};
