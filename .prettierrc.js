module.exports = {
  "trailingComma": "none",
  "printWidth": 100,
  "singleQuote": false,
  "wrapAttributes": false,
  "sortAttributes": false,
  "arrowParens": "avoid"
}