const redis = require("./redis");
const serialize = require("./serialize");
const search = require("./search");
const util = require("./util");
const sanitize = require("./sanitize");

function parse(data) {
  const isArray = Array.isArray(data);
  const iterate = isArray ? data : [data];

  const payload = [];
  for (const e of iterate) {
    try {
      const parsed = e ? JSON.parse(e) : null;
      payload.push(parsed);
    } catch (e) {
      payload.push(null);
    }
  }
  return isArray ? payload : payload[0];
}

module.exports = {
  ...redis,
  serialize,
  search,
  util,
  sanitize,
  parse
};
