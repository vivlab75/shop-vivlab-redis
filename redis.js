const { promisify } = require("util");
const redis = require("redis");
const client = redis.createClient({
  host: process.env.REDIS_HOST,
  port: process.env.REDIS_PORT || 6379,
  retry_strategy: () => 6000
});

const promise = command => promisify(client[command]).bind(client);

const SCAN = promise("SCAN");
const GET = promise("GET");
const MGET = promise("MGET");
const HGET = promise("HGET");
const HGETALL = promise("HGETALL");
const HKEYS = promise("HKEYS");
const HMGET = promise("HMGET");
const SET = promise("SET");
const MSET = promise("MSET");
const HSET = promise("HSET");
const HMSET = promise("HMSET");
const HEXISTS = promise("HEXISTS");
const DEL = promise("DEL");
const HDEL = promise("HDEL");
const FLUSHALL = promise("FLUSHALL");
const ZSCAN = promise("ZSCAN");
const ZADD = promise("ZADD");
const ZRANGE = promise("ZRANGE");
const ZREVRANGE = promise("ZREVRANGE");
const ZRANGEBYSCORE = promise("ZRANGEBYSCORE");
const ZRANGEBYLEX = promise("ZRANGEBYLEX");
const ZREVRANGEBYLEX = promise("ZREVRANGEBYLEX");
const ZREVRANGEBYSCORE = promise("ZREVRANGEBYSCORE");
const ZREMRANGEBYSCORE = promise("ZREMRANGEBYSCORE");
const ZPOPMAX = promise("ZPOPMAX");
const ZSCORE = promise("ZSCORE");
const ZLEXCOUNT = promise("ZLEXCOUNT");
const ZCOUNT = promise("ZCOUNT");
const ZINCRBY = promise("ZINCRBY");
const ZCARD = promise("ZCARD");
const ZREM = promise("ZREM");
const KEYS = promise("KEYS");
const RENAME = promise("RENAME");

const SADD = promise("SADD");
const SCARD = promise("SCARD");
const SISMEMBER = promise("SISMEMBER");
const SMEMBERS = promise("SMEMBERS");
const SREM = promise("SREM");


const RPUSH = promise("RPUSH");
const LRANGE = promise("LRANGE");
const EXPIRE = promise("EXPIRE");

const XADD = promise("XADD");
const XREVRANGE = promise("XREVRANGE");

const MULTI = (...commands) =>
  new Promise((resolve, reject) => {
    const chain = client.MULTI();
    for (const [command, ...args] of commands) {
      chain[command](...args);
    }
    chain.EXEC((err, res) => {
      if (err) reject(err);
      else resolve(res);
    });
  });

module.exports = {
  SCAN,
  GET,
  MGET,
  HGET,
  HGETALL,
  HKEYS,
  HMGET,
  HEXISTS,
  SET,
  MSET,
  HSET,
  HMSET,
  DEL,
  HDEL,
  FLUSHALL,
  ZSCAN,
  ZADD,
  ZRANGE,
  ZREVRANGE,
  ZRANGEBYSCORE,
  ZRANGEBYLEX,
  ZREVRANGEBYLEX,
  ZREVRANGEBYSCORE,
  ZREMRANGEBYSCORE,
  ZSCORE,
  ZLEXCOUNT,
  ZCOUNT,
  ZINCRBY,
  ZCARD,
  ZREM,
  ZPOPMAX,
  KEYS,
  RENAME,
  MULTI,
  XADD,
  XREVRANGE,
  RPUSH,
  LRANGE,
  EXPIRE,
  SADD,
  SCARD,
  SISMEMBER,
  SMEMBERS,
  SREM
};
