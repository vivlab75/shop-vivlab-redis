function calcDiscount(price, initial) {
  return Number(initial) ? Math.ceil((1 - price / initial) * -100) : 0;
}

function parseBoolean(bool) {
  return bool === "true" ? true : false;
}

function parseInteger(val) {
  if (val === null) {
    return null;
  }
  const int = parseInt(val);
  return isNaN(int) ? null : int;
}

function parseArray(obj) {
  try {
    return JSON.parse(obj);
  } catch (e) {
    return [];
  }
}

function parseObject(obj) {
  try {
    return JSON.parse(obj);
  } catch (e) {
    return {};
  }
}

module.exports = {
  calcDiscount,
  parseBoolean,
  parseInteger,
  parseArray,
  parseObject
};
