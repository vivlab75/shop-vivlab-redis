const sanitizeHtml = require("sanitize-html");

const tags = [
  "u",
  "b",
  "i",
  "em",
  "strong",
  "header",
  "h1",
  "h2",
  "h3",
  "h4",
  "h5",
  "h6",
  "br",
  "p",
  "ol",
  "ul",
  "li"
];

function html(e) {
  if (e === null || e === "" || e === undefined) return null;
  return sanitizeHtml(e, {
    allowedAttributes: {},
    selfClosing: ["br"],
    allowedTags: tags
  }).trim();
}

function htmlToText(e) {
  if (e === null || e === "" || e === undefined) return null;
  return e
    .replace(/<li>/gi, "- ")
    .replace(/<\/li>/gi, "\n")
    .replace(/<\/p>/gi, "\n")
    .replace(/<\/h\d>/gi, "\n")
    .replace(/<[\s/a-zA-Z0-9]+>/gi, "")
    .trim();
}

module.exports = {
  html,
  htmlToText
};
